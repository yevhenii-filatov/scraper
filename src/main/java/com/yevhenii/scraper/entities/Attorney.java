package com.yevhenii.scraper.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Attorney {
    private String photoUrl;
    private String vCardLink;
    private String name;
    private String firm;
    private String phone;
    private String website;
    private String address;
    private List<String> lawSchools;
    private List<String> sections;
    private List<String> categories;

    @Override
    public String toString () {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("%nName: %s%nPhoto URL: %s%nVisit Card URL: %s%nFirm: %s%nPhone: %s%nWebsite: %s%nAddress: : %s%nEducation: %n" ,
                name, photoUrl, vCardLink, firm, phone, website, address));

        for (String element: lawSchools) {
            builder.append(String.format("\t • %s%n", element));
        }

        builder.append(String.format("Sections: %n"));

        for (String element: sections) {
            builder.append(String.format("\t • %s%n", element));
        }

        builder.append(String.format("Categories: %n"));

        for (String element: categories) {
            builder.append(String.format("\t • %s%n", element));
        }

        builder.append(String.format("%n"));
        return builder.toString();
    }
}
