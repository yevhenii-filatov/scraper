package com.yevhenii.scraper.savers;

import com.yevhenii.scraper.entities.Attorney;
import com.yevhenii.scraper.utilities.Printer;
import lombok.AllArgsConstructor;

import java.sql.*;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class AttorneysDatabaseSaver {
    private Connection connection;

    private static final String INSERT_ATTORNEY_SCRIPT =
            "INSERT INTO attorneys (photoUrl, vCardLink, name, firm, phone, website, address) VALUES (?, ?, ?, ?, ?, ?, ?)";

    public boolean connectToDatabase() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://172.17.0.2/attorneys", "yevhenii", "123456789");
            Printer.printInformation("Connection successful");
            return true;
        } catch (SQLException e) {
            Printer.printError("Connection failed");
            Printer.printError(e.getMessage());
            return false;
        }
    }

    public void saveAttorneysList(List<Attorney> attorneys) {
        for (Attorney attorney : attorneys) {
            try {
                saveAttorney(attorney);
            } catch (SQLException ignored) {}
        }
    }


    private void saveAttorney(Attorney attorney) throws SQLException {
        if (connectionNonNull()) {
            int savedAttorneyId;

            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ATTORNEY_SCRIPT, Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, attorney.getPhotoUrl());
                preparedStatement.setString(2, attorney.getVCardLink());
                preparedStatement.setString(3, attorney.getName());
                preparedStatement.setString(4, attorney.getFirm());
                preparedStatement.setString(5, attorney.getPhone());
                preparedStatement.setString(6, attorney.getWebsite());
                preparedStatement.setString(7, attorney.getAddress());

                savedAttorneyId = getInsertedObjectId(preparedStatement, preparedStatement.executeUpdate());
            }

            this.saveLawSchools(attorney.getLawSchools(), savedAttorneyId);
            this.saveSections(attorney.getSections(), savedAttorneyId);
            this.saveCategories(attorney.getCategories(), savedAttorneyId);

        } else Printer.printError("Connection is null");
    }

    private void saveCategories(List<String> categoriesList, int attorneyId) throws SQLException {
        Set<Integer> savedCategoriesIds = saveListAndReturnIds(ListedInformation.CATEGORY, categoriesList);
        saveRelationsToAdditionalTable(RelationType.ATTORNEY_CATEGORY, savedCategoriesIds, attorneyId);
    }

    private void saveLawSchools(List<String> schoolsList, int attorneyId) throws SQLException {
        Set<Integer> savedSchoolsIds = saveListAndReturnIds(ListedInformation.LAW_SCHOOL, schoolsList);
        saveRelationsToAdditionalTable(RelationType.ATTORNEY_LAW_SCHOOL, savedSchoolsIds, attorneyId);
    }

    private void saveSections(List<String> sectionsList, int attorneyId) throws SQLException {
        Set<Integer> savedSectionsIds = saveListAndReturnIds(ListedInformation.SECTION, sectionsList);
        saveRelationsToAdditionalTable (RelationType.ATTORNEY_SECTION, savedSectionsIds, attorneyId);
    }

    private void saveRelationsToAdditionalTable(RelationType relationType, Set<Integer> savedListIds, int attorneyId) throws SQLException {
        for (Integer listItemId : savedListIds) {
            if (!saveRelation(relationType, listItemId, attorneyId)) {
                Printer.printError(String.format(
                        "Relation %s wasn't saved to DB.%nItem ID: %d%nAttorney ID: %d%n",
                        relationType, listItemId, attorneyId
                ));
            }
        }
    }

    private boolean saveRelation(RelationType relationType, int itemId, int attorneyId) throws SQLException {
        if (attorneyId == -1) return false;
        try (PreparedStatement statement = connection.prepareStatement(relationType.script)){
            statement.setInt(1, attorneyId);
            statement.setInt(2, itemId);

            return statement.executeUpdate() != 0;
        }
    }

    private Set<Integer> saveListAndReturnIds (ListedInformation type, List<String> list) throws SQLException {
        if (connectionNonNull()) {
            Set<Integer> idSet = new HashSet<>();

            for (String item : list) {
                int insertedObjectId = saveItemAndReturnIdOrJustGetId(type, item);
                if (insertedObjectId != -1) {
                    idSet.add(insertedObjectId);
                }
            }
            return idSet;
        }
        return new HashSet<>();
    }

    private int saveItemAndReturnIdOrJustGetId (ListedInformation itemType, String itemDescription) throws SQLException {
        int itemId = getItemIdFromDatabaseIfExists(itemType, itemDescription);
        if (itemId != -1) return itemId;

        return saveItemAndReturnId(itemType, itemDescription);
    }

    private int saveItemAndReturnId (ListedInformation type, String text) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(type.insertScript, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, text);

            int queryExecutionStatus = preparedStatement.executeUpdate();
            return getInsertedObjectId(preparedStatement, queryExecutionStatus);
        }
    }

    private int getItemIdFromDatabaseIfExists (ListedInformation itemType, String itemDescription) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(itemType.selectByTitleScript)) {
            preparedStatement.setString(1, itemDescription);
            preparedStatement.execute();

            try (ResultSet resultSet = preparedStatement.getResultSet()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                }
            }
        }
        return -1;
    }

    private int getInsertedObjectId (PreparedStatement statement, int queryExecutionStatus) throws SQLException {
        if (queryExecutionStatus != 0) {
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                }
            }
        }

        return -1;
    }

    private boolean connectionNonNull() {
        return Objects.nonNull(connection);
    }


    @AllArgsConstructor
    enum ListedInformation {
        LAW_SCHOOL(
                "INSERT INTO law_schools (school_title) VALUES (?)",
                "SELECT * FROM law_schools WHERE school_title = (?)"),
        SECTION("INSERT INTO sections (section_title) VALUES (?)",
                "SELECT * FROM sections WHERE section_title = (?)"),
        CATEGORY("INSERT INTO categories (category_title) VALUES (?)",
                "SELECT * FROM categories WHERE category_title = (?)");

        private String insertScript;
        private String selectByTitleScript;
    }

    @AllArgsConstructor
    enum RelationType {
        ATTORNEY_LAW_SCHOOL("INSERT INTO attorney_law_school VALUES (?, ?)"),
        ATTORNEY_SECTION("INSERT INTO attorney_section VALUES (?, ?)"),
        ATTORNEY_CATEGORY("INSERT INTO attorney_category VALUES (?, ?)");

        private String script;
    }
}
