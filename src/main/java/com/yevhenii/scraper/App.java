package com.yevhenii.scraper;

import com.yevhenii.scraper.entities.Attorney;
import com.yevhenii.scraper.savers.AttorneysDatabaseSaver;
import com.yevhenii.scraper.utilities.Parser;
import com.yevhenii.scraper.utilities.Printer;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class App  {
    public static void main( String[] args ) {
        try {
            List<Attorney> attorneys = new ArrayList<>();
            Parser.parseAttorneysCatalog(attorneys, 1, 1);
            Printer.printAttorneysCollection(attorneys);

            AttorneysDatabaseSaver saver = new AttorneysDatabaseSaver();
            if (saver.connectToDatabase()) {
                saver.saveAttorneysList (attorneys);
            }

        } catch (Exception e) {
            Printer.printError(e.getMessage());
        }
    }

}