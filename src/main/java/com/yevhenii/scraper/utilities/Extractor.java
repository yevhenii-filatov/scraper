package com.yevhenii.scraper.utilities;

import lombok.experimental.UtilityClass;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

@UtilityClass
public class Extractor {
    public static Elements getAttorneysCardsFromPage (Document page) {
        return page.body().getElementsByClass("directory-search-item");
    }

    public static Document getPage (String address) throws IOException {
        return Jsoup.connect(address).get();
    }

    public static Document loadPageViaLink (int pageNumber) throws IOException {
        if (pageNumber > 0) {
            return getPage(Links.FIRST_PAGE_URL.getUrl().concat("?page=" + pageNumber));
        }
        return getPage(Links.FIRST_PAGE_URL.getUrl());
    }


}
