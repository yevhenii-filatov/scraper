package com.yevhenii.scraper.utilities;

import lombok.Getter;

@Getter
public enum Links {
    WEBSITE_URL ("https://intus.austinbar.org/"),
    FIRST_PAGE_URL (WEBSITE_URL.getUrl().concat("directory/default.aspx"));

    private String url;

    Links(String url) {
        this.url = url;
    }

    @Override
    public String toString () {
        return url;
    }

    public static String buildFullUrlFromRelative(String relativePath) {
        return WEBSITE_URL.getUrl().concat(relativePath);
    }
}