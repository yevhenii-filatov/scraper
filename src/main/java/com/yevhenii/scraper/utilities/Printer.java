package com.yevhenii.scraper.utilities;

import com.yevhenii.scraper.entities.Attorney;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@UtilityClass
public class Printer {

    public static void printAttorneysCollection(List<Attorney> attorneys) {
        for (Attorney attorney : attorneys) printInformation(attorney.toString());
    }

    public static void printError(String errorText) {
        log.error(errorText);
    }

    public static void printInformation (String message) {
        log.info(message);
    }

}
