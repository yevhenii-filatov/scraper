package com.yevhenii.scraper.utilities;

import com.yevhenii.scraper.builders.AttorneyObjectParser;
import com.yevhenii.scraper.entities.Attorney;
import lombok.experimental.UtilityClass;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@UtilityClass
public class Parser {

    public static void parseAttorneysCatalog(List<Attorney> resultingCollection, int startPage, int endPage) throws IOException {
        for (int pageNumber = startPage; pageNumber <= endPage; pageNumber++) {
            parseCatalogPage(resultingCollection, pageNumber);
        }
    }

    private static void parseCatalogPage(List<Attorney> resultingCollection, int pageNumber) throws IOException {
        Document catalogPage = Extractor.loadPageViaLink(pageNumber);
        Elements cards = Extractor.getAttorneysCardsFromPage(catalogPage);
        List<String> linksToPersonalPages = parseCardsToGetLinks(cards);

        parseAttorneysPersonalPages(resultingCollection, linksToPersonalPages);
    }

    private static void parseAttorneysPersonalPages(List<Attorney> resultingCollection, List<String> links) throws IOException {
        for (String link : links) {
            parseAttorneyPageAndCreateObject(resultingCollection, link);
        }
    }

    private static void parseAttorneyPageAndCreateObject(List<Attorney> resultingCollection, String link) throws IOException {
        Document attorneyPage = Extractor.getPage(link);
        Element firstInfoColumn = attorneyPage.select("#ctl00_cphContent_eDirectoryDetails_divDetails").get(0);
        Element secondInfoColumn = attorneyPage.select("#ctl00_cphContent_eDirectoryDetails_divGroups").get(0);

        AttorneyObjectParser attorneyBuilder = new AttorneyObjectParser();
        attorneyBuilder.addPhotoUrl(attorneyPage);
        attorneyBuilder.addVCardLink(firstInfoColumn);
        attorneyBuilder.addName(firstInfoColumn);
        attorneyBuilder.addFirm(firstInfoColumn);
        attorneyBuilder.addPhone(firstInfoColumn);
        attorneyBuilder.addWebsite(firstInfoColumn);
        attorneyBuilder.addAddress(secondInfoColumn);
        attorneyBuilder.addEducationInfo(secondInfoColumn);
        attorneyBuilder.addSections(secondInfoColumn);
        attorneyBuilder.addCategories(secondInfoColumn);

        resultingCollection.add(attorneyBuilder.getAttorney());
    }

    private static List<String> parseCardsToGetLinks(Elements cards) {
        List<String> links = new ArrayList<>();
        for (Element card : cards) {
            links.add(parseCardForAttorneyPageLink(card));
        }

        return links;
    }

    private static String parseCardForAttorneyPageLink (Element attorneyCardFromCatalog) {
        Element htmlLinkToAttorneyPersonalPage = attorneyCardFromCatalog.getElementsByTag("a").first();
        String relativePath = parseHtmlLinkForHref(htmlLinkToAttorneyPersonalPage);
        return Links.buildFullUrlFromRelative(relativePath);
    }

    public static List<String> parseUnorderedList (Elements list) {
        ArrayList<String> result = new ArrayList<>();
        for (Element element : list) {
            result.add(parseElementForText(element));
        }
        return result;
    }

    public static String parseElementForText (Element element) {
        return element.text();
    }

    public static String parseHtmlLinkForHref (Element htmlLink) {
        return htmlLink.attr("href");
    }
}
