package com.yevhenii.scraper.builders;

import com.yevhenii.scraper.entities.Attorney;
import com.yevhenii.scraper.utilities.Links;
import com.yevhenii.scraper.utilities.Parser;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.List;

public class AttorneyObjectParser {
    private Attorney attorney;

    public AttorneyObjectParser() {
        attorney = new Attorney();
    }

    public Attorney getAttorney() {
        return attorney;
    }

    public void addPhotoUrl (Element blockToParse) {
        String relativePath = blockToParse.select("#ctl00_cphContent_eDirectoryDetails_imgImage").attr("src");
        attorney.setPhotoUrl(Links.buildFullUrlFromRelative(relativePath));
    }

    public void addVCardLink (Element blockToParse) {
        String relativePath = blockToParse.select("div:nth-child(1) > p > a").attr("href");
        attorney.setVCardLink(Links.buildFullUrlFromRelative(relativePath));
    }

    public void addName (Element blockToParse) {
        attorney.setName(blockToParse.select("div:nth-child(1) > p").text());
    }


    public void addFirm (Element blockToParse) {
        attorney.setFirm(blockToParse.select("div:nth-child(2) > p").text());
    }


    public void addPhone (Element blockToParse) {
        String phone = blockToParse.select("div:nth-child(3) > p > span").text();
        if (phone.equals("")) phone = blockToParse.select("div:nth-child(3) > p > a").text();
        attorney.setPhone(phone);
    }

    public void addWebsite (Element blockToParse) {
        attorney.setWebsite(blockToParse.select("div:nth-child(4) > p > a").attr("href"));
    }

    public void addAddress (Element blockToParse) {
        attorney.setAddress(blockToParse.select("div:nth-child(1) > address").text());
    }

    public void addEducationInfo(Element blockToParse) {
        Elements educationUl = blockToParse.select("div:nth-child(2) > ul");
        List<String> educationInfo = Parser.parseUnorderedList(educationUl);
        attorney.setLawSchools(educationInfo);
    }

    public void addSections(Element blockToParse) {
        Elements sectionsUl = blockToParse.select("div:nth-child(3) > ul");
        List<String> sections = Parser.parseUnorderedList(sectionsUl);
        attorney.setSections(sections);
    }

    public void addCategories(Element blockToParse) {
        Elements categoriesUl = blockToParse.select("div:nth-child(4) > ul");
        List<String> categories = Parser.parseUnorderedList(categoriesUl);
        attorney.setCategories(categories);
    }
}
